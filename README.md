# Elm Zip Benchmarks

Some benchmarks to test alternate implementations for the map functions in Elm's [`List`][List] module.

### General Idea

The map functions in the [`List`][List] module are implemented in two different ways.
The [`List.map`][List.map] function is defined in terms of [`List.foldr`][List.foldr], while [`List.map2`][List.map2], [`List.map3`][List.map3], [`List.map4`][List.map4], and [`List.map5`][List.map5] each have their own Javascript implementation that works by appending the result of each step to a Javascript array and then coverting to an Elm list once it has finished.
We suspected the neither of these implementations would be as efficient as one where the old list(s) were traversed and the resulting list created in the same pass.
The basic idea of our new implementation for these functions was that if we kept track of the current last element of the result list as it was being constructed, then if we needed to add another element after it, we could have the previously last element point to an element newly created using `_List_Cons` instead of `_List_Nil` which it would have been pointing to previously.
This would allow us to build the result list front to back, eliminating the need for second passes on the input list(s) or the result list.

## [`List.map`][List.map]

The [library version](https://github.com/elm/core/blob/84f38891468e8e153fc85a9b63bdafd81b24664e/src/List.elm#L123) of this function is implemented by calling foldr, which has to recursively traverse to the bottom of the list before it can start building up the result list.
Our version does not need to traverse all the way to the end of the input list before it starts creating the new list, using the method [described above](#general-idea).

Our version is fairly similar to the [Elm Optimize, Level 2 version](https://github.com/mdgriffith/elm-optimize-level-2/blob/8e61bed99f0df4713f49b9e60bfc42ec6867888f/src/replacements/list/$elm$core$List$map.js).
However, in our version we unroll the first iteration of the loop which seems to make our program run faster for smaller lists with comparable performance on larger lists.

#### Alternate `List.map`

```javascript
var _List_map = F2(
	function (f, xs) {
		if (xs.b) {
			var result = _List_Cons(f(xs.a), _List_Nil);
			var final = result;
			xs = xs.b;
			for (;xs.b; xs = xs.b) {
				final.b = _List_Cons(f(xs.a), _List_Nil);
				final = final.b;
			}
			return result;
		} else {
			return _List_Nil;
		}
	});

var $elm$core$List$map = _List_map;
```

#### `List.map` Results

[Try it yourself!](https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/listmap.html)
(`elm make` flags: `--optimize`)

| Platform                                                                                                                                     | Alternate Faster?  |
| --------                                                                                                                                     | -----------------  |
| [Firefox Version 78.15.0esr (64-bit), running on Debian 10.7 (64-bit) \[elm make --optimize\]][listmap_1]                                    | :heavy_check_mark: |
| [Chromium Version 90.0.4430.212 (Developer Build) built on Debian 10.9, running on Debian 10.11 (64-bit) \[elm make --optimize\]][listmap_2] | :heavy_check_mark: |

## [`List.map2`][List.map2]

The [library version](https://github.com/elm/core/blob/84f38891468e8e153fc85a9b63bdafd81b24664e/src/Elm/Kernel/List.js#L37) first constructs a Javasript array representing the result list, and then converts it into an Elm list.
Our version builds the result list as an Elm list without any intermediary steps, using the method [described above](#general-idea)

#### Alternate `List.map2`

```javascript
var _List_map2 = F3(
	function (f, xs, ys) {
		if (xs.b && ys.b) {
			var result = _List_Cons(A2(f, xs.a, ys.a), _List_Nil);
			var final = result;
			xs = xs.b;
			ys = ys.b;
			for (;xs.b && ys.b; xs = xs.b, ys = ys.b) {
				final.b = _List_Cons(A2(f, xs.a, ys.a), _List_Nil);
				final = final.b;
			}
			return result;
		} else {
			return _List_Nil;
		}
	});
```

#### `List.map2` Results

[Try it yourself!](https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/listmap2.html)
(`elm make` flags: `--optimize`)

| Platform                                                                                                                                      | Alternate Faster?  |
| --------                                                                                                                                      | -----------------  |
| [Firefox Version 78.15.0esr (64-bit), running on Debian 10.7 (64-bit) \[elm make --optimize\]][listmap2_1]                                    | :heavy_check_mark: |
| [Chromium Version 90.0.4430.212 (Developer Build) built on Debian 10.9, running on Debian 10.11 (64-bit) \[elm make --optimize\]][listmap2_2] | :heavy_check_mark: |

## [`List.map3`][List.map3]

The [library version](https://github.com/elm/core/blob/84f38891468e8e153fc85a9b63bdafd81b24664e/src/Elm/Kernel/List.js#L46) first constructs a Javasript array representing the result list, and then converts it into an Elm list.
Our version builds the result list as an Elm list without any intermediary steps, using the method [described above](#general-idea)

#### Alternate `List.map3`

```javascript
var _List_map3 = F4(
	function (f, xs, ys, zs) {
		if (xs.b && ys.b && zs.b) {
			var result = _List_Cons(A3(f, xs.a, ys.a, zs.a), _List_Nil);
			var final = result;
			xs = xs.b;
			ys = ys.b;
			zs = zs.b
			for (;xs.b && ys.b && zs.b; xs = xs.b, ys = ys.b, zs = zs.b) {
				final.b = _List_Cons(A3(f, xs.a, ys.a, zs.a), _List_Nil);
				final = final.b;
			}
			return result;
		} else {
			return _List_Nil;
		}
	});
```

#### `List.map3` Results

[Try it yourself!](https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/listmap3.html)
(`elm make` flags: `--optimize`)

| Platform                                                                                                                                      | Alternate Faster?  |
| --------                                                                                                                                      | -----------------  |
| [Firefox Version 78.15.0esr (64-bit), running on Debian 10.7 (64-bit) \[elm make --optimize\]][listmap3_1]                                    | :heavy_check_mark: |
| [Chromium Version 90.0.4430.212 (Developer Build) built on Debian 10.9, running on Debian 10.11 (64-bit) \[elm make --optimize\]][listmap2_2] | :heavy_check_mark: |

[List]: https://package.elm-lang.org/packages/elm/core/latest/List
[List.map]: https://package.elm-lang.org/packages/elm/core/latest/List#map
[List.foldr]: https://package.elm-lang.org/packages/elm/core/latest/List#foldr
[List.map2]: https://package.elm-lang.org/packages/elm/core/latest/List#map2
[List.map3]: https://package.elm-lang.org/packages/elm/core/latest/List#map3
[List.map4]: https://package.elm-lang.org/packages/elm/core/latest/List#map4
[List.map5]: https://package.elm-lang.org/packages/elm/core/latest/List#map5
[listmap_1]: https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/results/listmap/1.html
[listmap_2]: https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/results/listmap/2.html
[listmap2_1]: https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/results/listmap2/1.html
[listmap2_2]: https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/results/listmap2/2.html
[listmap3_1]: https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/results/listmap3/1.html
[listmap3_2]: https://e-neighborhood-watch.gitlab.io/elm/elm-zip-benchmarks/results/listmap3/2.html
