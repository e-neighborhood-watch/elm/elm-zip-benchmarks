module ListMap2 exposing
  ( main
  )


import Benchmark exposing
  ( Benchmark
  )
import Benchmark.Runner as Runner


import Util.List as List
import List


type alias InputInfo =
  { list1Length :
    Int
  , list2Length :
    Int
  }


inputInfoToString : InputInfo -> String
inputInfoToString { list1Length, list2Length } =
  "("
    ++ String.fromInt list1Length
    ++ ","
    ++ String.fromInt list2Length
    ++ ")"


type alias TestData =
  { inputInfo :
    InputInfo
  , list1 :
    List Int
  , list2 :
    List Int
  }


intTestData : Int -> Int -> TestData
intTestData list1Length list2Length =
  { inputInfo =
    { list1Length =
      list1Length
    , list2Length =
      list2Length
    }
  , list1 =
    List.repeat list1Length 278
  , list2 =
    List.repeat list2Length 2010
  }


ourMap2 : (a -> b -> c) -> List a -> List b -> List c
ourMap2 f a b =
  -- This will be replaced with a call to our custom List.map2
  List.map2 f a b


compareListMap2s : TestData -> Benchmark
compareListMap2s { inputInfo, list1, list2 } =
  Benchmark.compare
    ( inputInfoToString inputInfo
    )
    "Core library List.map2"
    ( \ _ -> List.map2 (+) list1 list2
    )
    "Our List.map2"
    ( \ _ -> ourMap2 (+) list1 list2
    )


makeTests : (Int -> Int -> TestData) -> List TestData
makeTests testData =
  ( List.singleton testData
    |> List.andMap [ 0, 1, 121 ]
    |> List.andMap [ 0, 1, 121 ]
  ) ++
    [ testData 11 11
    , testData 1331 1331
    , testData 14641 14641
    ]


intBenchmarks : List Benchmark
intBenchmarks =
  makeTests intTestData |> List.map compareListMap2s


main : Runner.BenchmarkProgram
main =
  (( intBenchmarks
  ))
    |> Benchmark.describe
      "List Map2 (addition on Ints). Info: (list 1 length; list 2 length)"
    |> Runner.program
