module ListMap3 exposing
  ( main
  )


import Benchmark exposing
  ( Benchmark
  )
import Benchmark.Runner as Runner


import Util.List as List
import List


type alias InputInfo =
  { list1Length :
    Int
  , list2Length :
    Int
  , list3Length :
    Int
  }


inputInfoToString : InputInfo -> String
inputInfoToString { list1Length, list2Length, list3Length } =
  "("
    ++ String.fromInt list1Length
    ++ ","
    ++ String.fromInt list2Length
    ++ ","
    ++ String.fromInt list3Length
    ++ ")"


type alias TestData =
  { inputInfo :
    InputInfo
  , list1 :
    List Int
  , list2 :
    List Int
  , list3 :
    List Int
  }


intTestData : Int -> Int -> Int -> TestData
intTestData list1Length list2Length list3Length =
  { inputInfo =
    { list1Length =
      list1Length
    , list2Length =
      list2Length
    , list3Length =
      list3Length
    }
  , list1 =
    List.repeat list1Length 278
  , list2 =
    List.repeat list2Length 2010
  , list3 =
    List.repeat list3Length 2981
  }


ourMap3 : (a -> b -> c -> d) -> List a -> List b -> List c -> List d
ourMap3 f a b c =
  -- This will be replaced with a call to our custom List.map3
  List.map3 f a b c


compareListMap3s : TestData -> Benchmark
compareListMap3s { inputInfo, list1, list2, list3 } =
  Benchmark.compare
    ( inputInfoToString inputInfo
    )
    "Core library List.map3"
    ( \ _ -> List.map3 (\x y z -> x + y + z) list1 list2 list3
    )
    "Our List.map3"
    ( \ _ -> ourMap3 (\x y z -> x + y + z) list1 list2 list3
    )


makeTests : (Int -> Int -> Int -> TestData) -> List TestData
makeTests testData =
  ( List.singleton testData
    |> List.andMap [ 0, 1, 121 ]
    |> List.andMap [ 0, 1, 121 ]
    |> List.andMap [ 0, 1, 121 ]
  ) ++
    [ testData 11 11 11
    , testData 1331 1331 1331
    , testData 14641 14641 14641
    ]


intBenchmarks : List Benchmark
intBenchmarks =
  makeTests intTestData |> List.map compareListMap3s


main : Runner.BenchmarkProgram
main =
  (( intBenchmarks
  ))
    |> Benchmark.describe
      "List Map3 (addition on Ints). Info: (list 1 length; list 2 length; list 3 length)"
    |> Runner.program
