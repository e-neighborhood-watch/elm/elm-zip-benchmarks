module ListMap exposing
  ( main
  )


import Benchmark exposing
  ( Benchmark
  )
import Benchmark.Runner as Runner


import Util.List as List
import List


type alias InputInfo =
  { listLength :
    Int
  , listType :
    String
  , functionName :
    String
  }


inputInfoToString : InputInfo -> String
inputInfoToString { listLength, listType, functionName } =
  "("
    ++ listType
    ++ ","
    ++ functionName
    ++ ","
    ++ String.fromInt listLength
    ++ ")"


type alias TestData a =
  { inputInfo :
    InputInfo
  , list :
    List a
  , function :
    a -> a
  }


intTestData : Int -> TestData Int
intTestData listLength =
  { inputInfo =
    { listLength =
      listLength
    , listType =
      "int"
    , functionName =
      "add 2"
    }
  , list =
    List.repeat listLength 278
  , function =
    (+) 2
  }


ourMap : (a -> c) -> List a -> List c
ourMap f a =
  -- This will be replaced with a call to our custom List.map
  List.map f a


eol2Map : (a -> c) -> List a -> List c
eol2Map f a =
  -- This will be replaced with a call to the existing Elm Optimize Level 2 improved List.map
  List.map f a


compareListMaps : TestData a -> Benchmark
compareListMaps { inputInfo, list, function } =
  Benchmark.describe
    ( inputInfoToString inputInfo
    )
    [ Benchmark.benchmark
      "Core library List.map"
      ( \ _ -> List.map function list
      )
    , Benchmark.benchmark
      "Elm Optimize Level 2 List.map"
      ( \ _ -> eol2Map function list
      )
    , Benchmark.benchmark
      "Our List.map"
      ( \ _ -> ourMap function list
      )
    ]


makeTests : (Int -> TestData a) -> List (TestData a)
makeTests testData =
  List.map testData
    [ 0
    , 1
    , 11
    , 121
    , 1331
    , 14641
    ]


intBenchmarks : List Benchmark
intBenchmarks =
  makeTests intTestData |> List.map compareListMaps


main : Runner.BenchmarkProgram
main =
  intBenchmarks
    |> Benchmark.describe
      "List Map. Info: (list type; function; list length)"
    |> Runner.program
