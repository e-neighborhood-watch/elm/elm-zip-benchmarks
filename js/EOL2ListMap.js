// From the repository of elm-optimize-level-2 by MD griffith, for purposes of comparison.
var $author$project$ListMap$eol2Map = F2(function (f, xs) {
    var tmp = _List_Cons(undefined, _List_Nil);
    var end = tmp;
    for (; xs.b; xs = xs.b) {
        var next = _List_Cons(f(xs.a), _List_Nil);
        end.b = next;
        end = next;
    }
    return tmp.b;
});
